import { Component, Input } from '@angular/core';

import { IEstablishment } from '../../models/establishment';

@Component( {
  selector: 'tr-establishment',
  templateUrl: './establishment.component.html',
  styleUrls: [ './establishment.component.scss' ]
} )
export class EstablishmentComponent {
  @Input() establishment: IEstablishment;
}
