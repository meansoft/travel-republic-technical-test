import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'tr-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss']
})
export class StarRatingComponent implements OnInit {
  @Input() rating: number;

  starArray: Array<any>;

  ngOnInit() {
    this.starArray = new Array(this.rating);
  }
}
