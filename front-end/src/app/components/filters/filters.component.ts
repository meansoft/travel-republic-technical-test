import { Component, EventEmitter, Output } from '@angular/core';

@Component( {
  selector: 'tr-filters',
  templateUrl: './filters.component.html',
  styleUrls: [ './filters.component.scss' ]
} )
export class FiltersComponent {
  @Output() starRatingsFilterChanged = new EventEmitter<Array<number>>();
  @Output() nameFilterChanged = new EventEmitter<string>();
  @Output() maximumCostFilterChanged = new EventEmitter<number>();
  @Output() minimumUserRatingChanged = new EventEmitter<number>();

  ratings = [ false, false, false, false, false ];

  onStarRatingChanged( $event, value ) {
    const ratingsAsNumbers = [];
    this.ratings[ value - 1 ] = $event;

    for ( let i = 0; i < this.ratings.length; i++ ) {
      if ( this.ratings[ i ] ) {
        ratingsAsNumbers.push( i + 1 );
      }
    }

    this.starRatingsFilterChanged.emit( ratingsAsNumbers );
  }

  onNameChanged( $event ) {
    this.nameFilterChanged.emit( $event );
  }

  onMaximumCostChanged( $event ) {
    this.maximumCostFilterChanged.emit( $event );
  }

  onMinimumUserRatingChanged( $event ) {
    this.minimumUserRatingChanged.emit( +$event.value );
  }
}
