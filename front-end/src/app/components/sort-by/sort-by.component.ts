import { Component, EventEmitter, Output } from '@angular/core';

@Component( {
  selector: 'tr-sort-by',
  templateUrl: './sort-by.component.html'
} )
export class SortByComponent {
  @Output() sortByChanged = new EventEmitter<string>();

  sortByOptions = [
    { value: '', viewValue: 'None' },
    { value: 'Distance ASC', viewValue: 'Distance (lowest first)' },
    { value: 'Distance DESC', viewValue: 'Distance (highest first)' },
    { value: 'Stars ASC', viewValue: 'Star rating (lowest first)' },
    { value: 'Stars DESC', viewValue: 'Star rating (highest first)' },
    { value: 'MinCost ASC', viewValue: 'Minimum cost (lowest first)' },
    { value: 'MinCost DESC', viewValue: 'Minimum cost (highest first)' },
    { value: 'UserRating ASC', viewValue: 'User rating (lowest first)' },
    { value: 'UserRating DESC', viewValue: 'User rating (highest first)' },
  ];

  selectChanged( $event ) {
    this.sortByChanged.emit( $event.value );
  }

}
