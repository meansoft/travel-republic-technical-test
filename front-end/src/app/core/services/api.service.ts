import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { environment } from '../../../environments/environment';

const PAGE_SIZE = 10;

@Injectable()
export class ApiService {

  public getEstablishiments = ( orderBy: string,
                                starRatingsFilter: Array<number>,
                                nameFilter: string,
                                maximumCostFilter: number,
                                minimumUserRatingFilter: number,
                                page: number ) => {
    const whereFilters: Array<string> = [];
    const filters: Array<string> = [];

    if ( starRatingsFilter.length ) {
      whereFilters.push( `{"Stars":{"inq":[${starRatingsFilter}]}}` );
    }

    if ( nameFilter ) {
      whereFilters.push( `{"Name":{"like":"${nameFilter}.*", "options":"i"}}` );
    }

    if ( maximumCostFilter > 0 ) {
      whereFilters.push( `{"MinCost":{"lte":"${maximumCostFilter}"}}` );
    }

    if ( minimumUserRatingFilter > 0 ) {
      whereFilters.push( `{"UserRating":{"gte":"${minimumUserRatingFilter}"}}` );
    }

    if ( whereFilters.length ) {
      const whereFiltersString = `"where": {"and": [${whereFilters.join( ',' )}]}`;
      filters.push( whereFiltersString );
    }

    if ( orderBy ) {
      filters.push( `"order":"${orderBy}"` );
    }

    filters.push( `"limit":"${PAGE_SIZE}"` );

    // Unfortunately, LoopBack does not return the total number of results that match
    // filter criteria, so pagination is difficult
    filters.push( `"skip":"${page * PAGE_SIZE}"` );

    const filterString = `?filter={${filters.join( ',' )}}`;

    return this.http.get( `${environment.apiRoot}Establishments${filterString}` );
  }

  constructor( private http: Http ) {
  }
}
