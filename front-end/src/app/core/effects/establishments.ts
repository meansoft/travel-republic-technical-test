import { Actions, Effect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import '../../rxjs-operators';
import { ActionTypes, loadAllEstablishmentsSuccess, loadMoreEstablishmentsSuccess } from '../actions/establishments';
import { ApiService } from '../services/api.service';
import { IState } from '../../reducers/index';

@Injectable()
export class EstablishmentsEffects {
  @Effect()
  loadAllEstablishments$ =
    this.actions$
        .ofType(
          ActionTypes.CHANGE_STAR_RATINGS_FILTER,
          ActionTypes.CHANGE_ORDER_BY,
          ActionTypes.CHANGE_NAME_FILTER,
          ActionTypes.CHANGE_MAXIMUM_COST_FILTER,
          ActionTypes.CHANGE_MINIMUM_USER_RATING_FILTER
        )
        .withLatestFrom(
          this.store.select( ( state: IState ) => state.establishments.orderBy ),
          this.store.select( ( state: IState ) => state.establishments.starRatingsFilter ),
          this.store.select( ( state: IState ) => state.establishments.nameFilter ),
          this.store.select( ( state: IState ) => state.establishments.maximumCostFilter ),
          this.store.select( ( state: IState ) => state.establishments.minimumUserRatingFilter )
        )
        .switchMap( ( [ action, orderBy, starRatingsFilter, nameFilter, maximumCostFilter, minimumUserRatingFilter ] ) => {
          return this.apiService
                     .getEstablishiments( orderBy, starRatingsFilter, nameFilter, maximumCostFilter, minimumUserRatingFilter, 0 )
                     .map( ( response ) => response.json() )
                     .map( loadAllEstablishmentsSuccess );
        } );

  @Effect()
  loadMoreResults$ =
    this.actions$
        .ofType( ActionTypes.LOAD_MORE_ESTABLISHMENTS )
        .withLatestFrom(
          this.store.select( ( state: IState ) => state.establishments.orderBy ),
          this.store.select( ( state: IState ) => state.establishments.starRatingsFilter ),
          this.store.select( ( state: IState ) => state.establishments.nameFilter ),
          this.store.select( ( state: IState ) => state.establishments.maximumCostFilter ),
          this.store.select( ( state: IState ) => state.establishments.minimumUserRatingFilter ),
          this.store.select( ( state: IState ) => state.establishments.currentPage )
        )
        .switchMap( ( [ action, orderBy, starRatingsFilter, nameFilter, maximumCostFilter, minimumUserRatingFilter, currentPage ] ) => {
          return this.apiService
                     .getEstablishiments( orderBy, starRatingsFilter, nameFilter, maximumCostFilter, minimumUserRatingFilter, currentPage )
                     .map( ( response ) => response.json() )
                     .map( loadMoreEstablishmentsSuccess );
        } );

  constructor( private actions$: Actions,
               private store: Store<IState>,
               private apiService: ApiService ) {
  }
}
