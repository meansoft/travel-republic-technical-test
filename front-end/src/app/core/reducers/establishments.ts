import { Action } from '@ngrx/store';

import { IEstablishment } from '../../models/establishment';
import * as establishments from '../actions/establishments';
import {
  IChangeMaximumCostFilterAction,
  IChangeMinimumUserRatingAction,
  IChangeNameAction,
  IChangeOrderByAction,
  IChangeStarRatingsFilterAction,
  ILoadAllEstablishmentsSuccessAction
} from '../actions/establishments';

export interface IState {
  establishments: Array<IEstablishment>;
  orderBy: string;
  starRatingsFilter: Array<number>;
  nameFilter: string;
  maximumCostFilter: number;
  minimumUserRatingFilter: number;
  resultsLoaded: boolean;
  currentPage: number;
}

const initialState: IState = {
  establishments: [],
  orderBy: undefined,
  starRatingsFilter: [],
  nameFilter: undefined,
  maximumCostFilter: 0,
  minimumUserRatingFilter: 0,
  resultsLoaded: false,
  currentPage: 0
};

export function reducer( state = initialState, action: Action ): IState {
  switch ( action.type ) {
    case establishments.ActionTypes.LOAD_ALL_SUCCESS:
      return {
        ...state,
        establishments: (action as ILoadAllEstablishmentsSuccessAction).payload.establishments,
        resultsLoaded: true
      };

    case establishments.ActionTypes.CHANGE_STAR_RATINGS_FILTER:
      return {
        ...state,
        starRatingsFilter: (action as IChangeStarRatingsFilterAction).payload.newStarRatings,
        currentPage: 0
      };

    case establishments.ActionTypes.CHANGE_ORDER_BY:
      return {
        ...state,
        orderBy: (action as IChangeOrderByAction).payload.newOrderBy,
        currentPage: 0
      };

    case establishments.ActionTypes.CHANGE_NAME_FILTER:
      return {
        ...state,
        nameFilter: (action as IChangeNameAction).payload.newName,
        currentPage: 0
      };

    case establishments.ActionTypes.CHANGE_MAXIMUM_COST_FILTER:
      return {
        ...state,
        maximumCostFilter: (action as IChangeMaximumCostFilterAction).payload.newMaximumCost,
        currentPage: 0
      };

    case establishments.ActionTypes.CHANGE_MINIMUM_USER_RATING_FILTER:
      return {
        ...state,
        minimumUserRatingFilter: (action as IChangeMinimumUserRatingAction).payload.newMinimumUserRating,
        currentPage: 0
      };


    case establishments.ActionTypes.LOAD_MORE_ESTABLISHMENTS:
      return {
        ...state,
        currentPage: state.currentPage + 1
      };

    case establishments.ActionTypes.LOAD_MORE_ESTABLISHMENTS_SUCCESS:
      return {
        ...state,
        establishments: [
          ...state.establishments,
          ...(action as ILoadAllEstablishmentsSuccessAction).payload.establishments
        ]
      };

    default:
      return state;
  }
}
