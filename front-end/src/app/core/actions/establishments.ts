import { Action } from '@ngrx/store';

import { IEstablishment } from '../../models/establishment';

export const ActionTypes = {
  LOAD_ALL: '[Establishments] Load All',
  LOAD_ALL_SUCCESS: '[Establishments] Load All Success',
  CHANGE_ORDER_BY: '[Establishments] Change Order By',
  CHANGE_STAR_RATINGS_FILTER: '[Establishments] Change Star Ratings Filter',
  CHANGE_NAME_FILTER: '[Establishments] Change Name Filter',
  CHANGE_MAXIMUM_COST_FILTER: '[Establishments] Change Maximum Cost Filter',
  CHANGE_MINIMUM_USER_RATING_FILTER: '[Establishments] Change Minimum User Rating Filter',
  LOAD_MORE_ESTABLISHMENTS: '[Establishments] Load More Establishments',
  LOAD_MORE_ESTABLISHMENTS_SUCCESS: '[Establishments] Load More Establishments Success'
};

export interface ILoadAllEstablishmentsSuccessAction extends Action {
  payload: {
    establishments: Array<IEstablishment>
  };
}

export function loadAllEstablishmentsSuccess( establishments: Array<IEstablishment> ): ILoadAllEstablishmentsSuccessAction {
  return {
    type: ActionTypes.LOAD_ALL_SUCCESS,
    payload: {
      establishments
    }
  };
}

export interface IChangeStarRatingsFilterAction extends Action {
  payload: {
    newStarRatings: Array<number>
  };
}

export function changeStarRatingsFilter( newStarRatings: Array<number> ): IChangeStarRatingsFilterAction {
  return {
    type: ActionTypes.CHANGE_STAR_RATINGS_FILTER,
    payload: {
      newStarRatings
    }
  };
}

export interface IChangeOrderByAction extends Action {
  payload: {
    newOrderBy: string
  };
}

export function changeOrderBy( newOrderBy: string ): IChangeOrderByAction {
  return {
    type: ActionTypes.CHANGE_ORDER_BY,
    payload: {
      newOrderBy
    }
  };
}

export interface IChangeNameAction extends Action {
  payload: {
    newName: string
  };
}

export function changeNameFilter( newName: string ): IChangeNameAction {
  return {
    type: ActionTypes.CHANGE_NAME_FILTER,
    payload: {
      newName
    }
  };
}

export interface IChangeMaximumCostFilterAction extends Action {
  payload: {
    newMaximumCost: number
  };
}

export function changeMaximumCostFilter( newMaximumCost: number ): IChangeMaximumCostFilterAction {
  return {
    type: ActionTypes.CHANGE_MAXIMUM_COST_FILTER,
    payload: {
      newMaximumCost
    }
  };
}

export interface IChangeMinimumUserRatingAction extends Action {
  payload: {
    newMinimumUserRating: number
  };
}

export function changeMinimumUserRatingFilter( newMinimumUserRating: number ): IChangeMinimumUserRatingAction {
  return {
    type: ActionTypes.CHANGE_MINIMUM_USER_RATING_FILTER,
    payload: {
      newMinimumUserRating
    }
  };
}

export function loadMoreResults(): Action {
  return {
    type: ActionTypes.LOAD_MORE_ESTABLISHMENTS
  };
}

export interface ILoadMoreEstablishmentsSuccessAction extends Action {
  payload: {
    establishments: Array<IEstablishment>
  };
}

export function loadMoreEstablishmentsSuccess( establishments: Array<IEstablishment> ): ILoadMoreEstablishmentsSuccessAction {
  return {
    type: ActionTypes.LOAD_MORE_ESTABLISHMENTS_SUCCESS,
    payload: {
      establishments
    }
  };
}
