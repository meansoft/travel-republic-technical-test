import { Component, ElementRef } from '@angular/core';

@Component( {
  selector: 'tr-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
} )
export class AppComponent {
  constructor( private element: ElementRef ) {
  }

  toggleFullscreen() {
    const elem = this.element.nativeElement.querySelector( '.tr-content' );

    if ( elem.requestFullscreen ) {
      elem.requestFullscreen();
    } else if ( elem.webkitRequestFullScreen ) {
      elem.webkitRequestFullScreen();
    } else if ( elem.mozRequestFullScreen ) {
      elem.mozRequestFullScreen();
    } else if ( elem.msRequestFullScreen ) {
      elem.msRequestFullScreen();
    }
  }
}
