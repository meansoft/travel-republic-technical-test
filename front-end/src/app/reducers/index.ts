import { ActionReducer, ActionReducerMap, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';

import { storeFreeze } from 'ngrx-store-freeze';

import * as fromEstablishments from '../core/reducers/establishments';
import { environment } from '../../environments/environment';

export interface IState {
  establishments: fromEstablishments.IState;
}

export const reducers: ActionReducerMap<IState> = {
  establishments: fromEstablishments.reducer
};

export function logger( reducer: ActionReducer<IState> ): ActionReducer<IState> {
  return ( state: IState, action: any ): IState => {
    console.log( 'state', state );
    console.log( 'action', action );

    return reducer( state, action );
  };
}

export const metaReducers: MetaReducer<IState>[] = !environment.production
  ? [ logger, storeFreeze ]
  : [];

export const getEstablishmentsState = createFeatureSelector<fromEstablishments.IState>( 'establishments' );

export const getEstablishments = createSelector(
  getEstablishmentsState,
  ( state: fromEstablishments.IState) => state.establishments
);

export const getResultsLoaded = createSelector(
  getEstablishmentsState,
  (state: fromEstablishments.IState) => state.resultsLoaded
);
