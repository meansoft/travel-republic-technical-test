import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home.page/home.page';
import { AngularMaterialModule } from './angular-material.module';
import { metaReducers, reducers } from './reducers';
import { EstablishmentsEffects } from './core/effects/establishments';
import { ApiService } from './core/services/api.service';
import { EstablishmentComponent } from './components/establishment/establishment.component';
import { StarRatingComponent } from './components/star-rating/star-rating.component';
import { FiltersComponent } from './components/filters/filters.component';
import { SortByComponent } from './components/sort-by/sort-by.component';
import { FormsModule } from '@angular/forms';

@NgModule( {
  declarations: [
    AppComponent,
    HomePageComponent,
    EstablishmentComponent,
    StarRatingComponent,
    FiltersComponent,
    SortByComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    FormsModule,
    RouterModule.forRoot( [
      { path: '', component: HomePageComponent, pathMatch: 'full' }
    ] ),
    StoreModule.forRoot( reducers, { metaReducers } ),
    EffectsModule.forRoot( [ EstablishmentsEffects ] ),
    AngularMaterialModule,
    FlexLayoutModule
  ],
  providers: [ ApiService ],
  bootstrap: [ AppComponent ]
} )
export class AppModule {}
