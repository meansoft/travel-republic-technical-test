import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';

import * as fromRoot from '../../reducers';
import * as establishments from '../../core/actions/establishments';
import { IEstablishment } from '../../models/establishment';

@Component( {
  selector: 'tr-home-page',
  templateUrl: './home.page.html',
  styleUrls: [ './home.page.scss' ]
} )
export class HomePageComponent implements OnInit {
  establishments$: Observable<Array<IEstablishment>>;
  resultsLoaded$: Observable<boolean>;

  constructor( private store: Store<fromRoot.IState> ) {
    this.establishments$ = this.store.select( fromRoot.getEstablishments );
    this.resultsLoaded$ = this.store.select( fromRoot.getResultsLoaded );
  }

  ngOnInit() {
  }

  onSortByChanged( columnName: string ) {
    this.store.dispatch( establishments.changeOrderBy( columnName ) );
  }

  onStarRatingsFilterChanged( $event ) {
    this.store.dispatch( establishments.changeStarRatingsFilter( $event ) );
  }

  onNameFilterChanged( $event ) {
    this.store.dispatch( establishments.changeNameFilter( $event ) );
  }

  onMaximumCostFilterChanged( $event ) {
    this.store.dispatch( establishments.changeMaximumCostFilter( +$event ) );
  }

  onMinimumUserRatingChanged( $event ) {
    this.store.dispatch( establishments.changeMinimumUserRatingFilter( $event ) );
  }

  onLoadMoreClicked() {
    this.store.dispatch( establishments.loadMoreResults() );
  }
}
