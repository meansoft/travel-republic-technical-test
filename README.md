# Travel Republic technical test

Thank you for giving me the opportunity to complete this technical test. I spent much longer on it that I had intended
to but really enjoyed doing it!

## Setup

The project was built using Angular 4.2.4 on the front end, and LoopBack 3 on the back end. It requires `yarn v1.0.1` to be
installed. You must clone the entire repository and then execute the following commands to launch the API:

```
cd api
yarn
yarn start
```

To launch the front end, in a separate window:

```
cd front-end
yarn
yarn start
```

Then you can view the site by navigating to `http://localhost:4200`

## Browser compatibility

I have tested the app on the following browsers:

### Mac

* Chrome Version 61.0.3163.100
* Safari Version 11.0.1 (12604.3.5.1.1)
* Firefox Version 56.0

### Windows

Unfortunately, I don't have access to Windows at home.

## Missing features and improvements that could be made

I spent quite a long time on the site — around eight hours — and I didn't think I should spend more. But there are still
numerous ways in which the site could be improved:

* There are no unit or end-to-end tests
* The Redux implementation could use more feature selectors
* The CSS could follow a more standard implementation, such as BEM
* The 'Load More' button isn't great. Unfortunately LoopBack doesn't provide a count of the number of records that mach
the filters so pagination is not as elegant as it could be
* The Redux implementation on the filters and ordering could be bound bi-directionally

I'm quite happy with the results though. Enjoy!

Mark
<mark.norgate@meansoft.co.uk> 